Title:			NX nutzen ohne großen Aufwand
Tags:			NX, SimpleNX
Allow-Comments:	yes
Image:			tux.svg
DateTime:		Mon, 27 May 2012 03:50:00 CET
UUID:			7beff8d0e04d1817e04e5021ad53bf4f
URL:	articles/2012-05/nx-nutzen-ohne-grossen-aufwand.html

[preview]
Wer viel unterwegs ist, wünscht sich häufiger mal, er könnte direkt auf seine Programme und Dateien zu Hause zugreifen. Linux kennt hierfür viele verschiedene Wege, ganz vorne SSH. In diesem Beitrag werden wir euch zeigen, wie mit einem simplen Shellscript die Grundfunktionalität von NX erhalten kann, ohne den kompletten Client und Server zu installieren.

Wenn es um grafische Anwendungen geht, kommt SSH schnell an seine Grenzen – es unterstützt zwar das Tunneln des X Protokolls, dies alleine ist aber ungeeignet bei langsamen Verbindungen über der das Internet - es fehlt einfach eine passende Kompressions-Schicht. Genau diesem Problem hat sich die Firma [url="http://www.nomachine.com/"]NoMachine[/url] angenommen. Sie haben einen eigenen XServer entwickelt bei dem die Ausgabe alle dargestellten Anwendungen komprimiert und an den verbundenen Client geschickt werden.

Der NX Server ist leider kommerziell und die kostenlose Version hat einige Einschränkungen. Da aber die Kernkomponenten von NX als Open Source [url="http://www.nomachine.com/sources.php"]zur Verfügung stehen[/url], haben sich einige freie Server Implementierungen entwickelt, z.B. [url="http://freenx.berlios.de/"]FreeNX[/url] oder [url="http://code.google.com/p/neatx/"]NeatX[/url]. Diese sind mit dem offiziellen Client kompatibel und nutzen (wie das Original) eine fragliche Authentifizierungsmethode. Beim Server muss ein Benutzer hinzugefügt werden, welcher über SSH erreichbar sein muss und dessen Key im Client einprogrammiert ist. Es kann sich also jeder von außen mit dem öffentlich verfügbaren Key in den Server einloggen, und bekommt dann ein Terminal, in welchem sich der Benutzer noch einmal mit seinen echten Login-Daten authentifizieren muss.
[/preview]

Bei NeatX wird das ganze dadurch abgesichert, das die Login Shell des NX Benutzers modifiziert ist, und ein Benutzer nach der Eingabe seiner echten Login-Daten mittels “su” autorisiert. Dies mag zwar in der Theorie sicher sein, aber bei einem Fehler in dem zuständigen Programm kann sich jeder in das Server-System einloggen. Desweiteren ist eine Autorisierung der Nutzer mittels individueller Keyfiles ebenfalls nicht möglich. Ich wollte nun diese Technik auf meinem [url="https://wiki.ubuntu.com/ARM/TEGRA/AC100"]Toshiba AC100[/url] mit Ubuntu nutzen und da es hierfür keinen offiziellen Client gibt (das Netbook besitzt einen ARM Prozessor), habe ich nach einer Alternative gesucht und einen interessanten Ansatz gefunden.

Es handelt sich dabei um ein Shell Script namens SimpleNX, dieses verbindet sich unter Verwendung seines eigenen Benutzer Accounts zu einem SSH-Server und startet dort den nxagent. Danach werden die Daten über die SSH Verbindung getunnelt und an den Clienten zurück geschickt. Auf diesem wird das Programm nxproxy gestartet und schon ist es möglich, auf dem Server Programme zu starten und am Client zu bedienen. Die beiden erwähnten Programme sind Open Source und zumindestens nxproxy befindet sich in den Paketquellen von Ubuntu. Das Script wurde von Miguel Angel Alvarez für das Smartphone Betriebssystem [url="http://de.wikipedia.org/wiki/Maemo"]Maemo[/url] entwickelt und aus einem der Source Paket von [url="http://maemo.org/packages/view/snx/"]hier[/url] extrahiert.

Mit nur wenigen Anpassungen ist das Script auch auf Ubuntu/Debian lauffähig. Die modifizierte Version steht (wie das Original) unter der GPL Version 3.

[download]
[* url="/cms/downloads/snx.sh" md5="c7b817e4918e3f0fda1df70104c15648" sha256="54ade518a1a7e5781e9ed874c9bae21321263970994736e56ec46aaa5ff67dd5" size="2,7 kb"]SimpleNX Script
[/download]

Um es zu Benutzen, müssen ggf. je nach Konfiguration die Server Pfade in Zeile 25 angepasst werden:
[code=bash]NXCMD="LD_LIBRARY_PATH=/usr/X11R6/lib/ /usr/X11R6/bin/nxagent"[/code]
Nun aber mal ein kurzer Vergleich zwischen dem Original Server/Client und der Script version:

Vorteile:
[list]
[*]kein neuer Benutzer erforderlich, zu dem prinzipiell jeder Zugang hat
[*]kein dauernd aktiver Server Prozess notwendig
[*]funktioniert auch auf Plattformen welche nicht vom offiziellen Client unterstützt werden
[*]die Authentifizierung kann ohne großen Aufwand auf Private Key umgestellt werden
[/list]

Nachteile: 

[list]
[*]alle Anwendungen werden terminiert sobald die Verbindung beendet wird (der Original Server lässt die Anwendungen weiter laufen)
[*]Es wird nur die GUI übertragen, der Original-Server unterstützt noch viele weitere Funktionen wie Sound-Übertragung, mounten von Verzeichnissen etc.
[/list]

Ob dieses Variante nun für einen in Frage kommt ist abhängig von der Benutzung: Für mich, der nur ein mal schnell ein Programm starten will, welches nicht unter ARM Prozessoren funktioniert oder auf seine privaten Daten zugreifen will ist diese Lösung perfekt. Soll jedoch sein ganzer Desktop mit allen geöffneten Programmen zur Verfügung stehen, so sollte lieber auf den Original NX Server oder eine freie Implementierung von diesem zurückgegriffen werden.

[info]
Falls ihr das Skript verwendet und euch die Schrift zu klein/groß ist, dann müsst ihr in Zeile 49:
[code=bash firstline=49]DPI=$(xdpyinfo | grep -m 1 "resolution:" | sed 's/^.* \([0-9]\+\)x.*$/\1/')[/code]
manuell eine andere DPI anzahl eintragen. Das Skript versucht die DPI Anzahl eures Monitors auszuelesen und dem Server mitzuteilen, damit dieser die Schrift passend zu euerem Display rendern kann. Falls ihr dieses Skript jedoch z.B. auf einem mobilen Gerät benutzt, bei dem die zurückgegebene DPI Anzahl nicht stimmt, kann dies zu einer sehr kleinen Schrift führen und ihr müsst dies manuell korrigieren.
[/info]

Ich hoffe ich konnte euch damit eine einfache und schnelle Variante zeigen, wie NX auch ohne Extra-Server und neue Benutzer verwendet werden kann.

Die Zukunft von freien NX Varianten ist momentan jedoch recht ungewiss, da NoMachine die Open-Source-Komponenten mit der nächsten Version [url="http://www.linux-magazin.de/NEWS/Nomachine-wechselt-mit-NX-4.0-zu-Closed-Source-Lizenz"]unter eine Closed Source Lizenz stellen wird[/url].