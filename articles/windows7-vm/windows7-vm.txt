Title:			Windows 7 Partition in Virtualbox / KVM / VMware Player unter Linux starten
Tags:			Dualboot, fdisk, GRUB, KVM, Master Boot Record, MBR, Partition, RAID, Virtualbox, VM, Windows, VMware Player
Allow-Comments:	yes
Image:			hdd2.svg
DateTime:		Tue, 7 Aug 2012 22:55:00 CET
UUID:			660b0f832270b232b3180b4febd705cd
URL:	articles/2012-08/windows-7-partition-in-virtualbox-kvm-vmware-player-unter-linux.html

[info]There is also an english translation of this article available [aurl=windows7-vm-en.txt]here[/aurl][/info]

[preview]
Jeder Linux Nutzer kennt das Problem: Es gibt nicht für jedes Programm oder jeden Service eine äquivalente Alternative für Linux, und es muss auf Windows (oder Wine) zurückgegriffen werden. Um wenigstens nicht ganz auf seine gewohnte Desktopumgebung verzichten zu müssen, wird Windows einfach in einer VM wie Virtualbox oder KVM installiert. Dies funktioniert so lange problemlos, wie keine aufwendigen Spiele oder ähnlich 3D lastige Software genutzt werden soll. In diesem Fall geht kein Weg an einer extra Windows Partition vorbei, und es endet oft mit zwei Windows Installationen die einfach nur unnötig Platz wegnehmen. In diesem Artikel werde ich zeigen, wie ein bereits installiertes Windows 7 in Virtualbox gestartet werden kann und somit nur noch eine Installation benötigt wird, die sowohl real wie auch in einer VM gebootet werden kann.

Aus technischer Sicht funktioniert diese “Dualboot” Methode problemlos, jedoch ist es schwierig das Ganze in Übereinstimmung mit der Lizenz-Politik von Windows 7 umzusetzen. Microsoft verlangt den Kauf einer zweiten Lizenz wenn Windows 7 zusätzlich in einer VM installiert werden soll. In diesem Fall ist dies jedoch etwas unsinnig, da es sich lediglich um [u]eine[/u] Installation handelt, welche nicht mehrfach gleichzeitig genutzt werden kann. Selbst wenn eine weitere Lizenz erworben wird, hilft das recht wenig im Bezug auf die Aktivierung von Windows, denn Windows 7 kennt keine Möglichkeit mehrere Lizenzen in einer Installation zu verwalten. Dies führt dazu, dass Windows mit hoher Wahrscheinlichkeit jedes Mal neu aktiviert werden muss, wenn zwischen den beiden Boot-Varianten wechselt wird, da sich aus Sicht des Betriebssystems zu viele Hardwarekomponenten geändert haben. 
[/preview]

[warning]Aufgrund dieser Lizenz-Problematik ist die Anleitung nur als Proof-of-Concept zu verstehen.[/warning]

Bevor wir starten, möchte ich noch kurz darauf eingehen, warum es sich lohnt diese doch etwas komplexere Anleitung zu befolgen und nicht eine der anderen (meistens einfacheren), die im Internet verfügbar sind.

Die meisten anderen Anleitungen erstellen einen neuen Master Boot Record (MBR), welcher eine neue Partitionstabelle beinhaltet. Dieser MBR stimmt in großen Teilen mit dem original MBR überein, die Windows Partionen befinden sich daher an der selben Stelle, jedoch werden nicht alle Informationen übernommen. Windows schreibt bei der Installation eine sogennante Disk Signature in den MBR um die Festplatte später eindeutig identifizieren zu können. Dies hilft Windows (bzw. dem Bootloader) mehrere Installationen unterscheiden zu können. Dieses Problem wird in den meisten Anleitungen vernachlässigt, was dazu führt, dass Windows seine Installation nicht mehr finden kann und eine Windows DVD benötigt wird, deren Reperaturfunktion diesen Umstand wieder gerade biegen soll. Je nach dem ob nun die Disk Signature im MBR oder in Windows Installation angepasst wird, ist diese Reparatur nach jedem Wechsel zwischen realem Booten und der VM nötig. In meiner Anleitung werden wir unseren virtuellen MBR so anpassen, dass die Werte übereinstimmen und die Reperaturfunktion von Windows unangetastet bleibt.

Falls Windows nicht auf einer eigenen Partition, sondern auf einer eigenen Festplatte installiert ist, besteht dieses Problem nicht, da in diesem Fall der Original MBR übernommen werden kann (so lange sich Window 7 und die Windows 7 Boot Partition auf der selben Festplatte befinden)

Ein weiterer Vorteil dieser Anleitung ist, das sie nahezu unabhängig von der Virtualisierungssoftware ist. Es werden nur Linux Befehle und eine Windows DVD benutzt (welche nicht unbedingt notwendig wäre) um eine Virtuelle Festplatte zu erzeugen, die nur noch in die Virtualisierungssoftware der Wahl eingebunden werden muss. Andere Anleitungen basieren in der Regel auf speziellen Befehlen von Virtualbox und funktionieren somit nur bei einer bestimmten VM. In diesem Fall, wird die Arbeit zur Erstellung der Partitionstabelle Virtualbox überlassen, was dazu führt, dass Virtualbox bei jedem Starten der VM kompletten Lesezugriff auf die Festplatte benötigt, um die reale Partitionstabelle zu lesen. Dazu benötigt der Benutzer, welcher Virtualbox gestartet hat, vollständigen Lesezugriff auf die gesamte Festplatte. Dies stellt eine nicht zu vernachlässigende Sicherheitslücke dar, da mit geeigneten Programmen alle Dateien auf allen Partitionen des Laufwerks gelesen werden können, unabhängig von den normalen Berechtigungen des Benutzers. In dieser Anleitung sind lediglich dauerhafte (während Windows in der VM läuft) Schreib und Leserechte für die Windows Partitionen nötig.

[section]Vorbereitung[/section]
Wenn trotz der oben gennanten Lizenzproblematik Interesse an dieser Anleitung besteht, so empfehle ich dringend ein Backup der Windows Partition anzulegen. Ein kleiner Fehler kann das ganze Dateisystem von Windows irreparabel zerstören und somit alle Daten unbrauchbar machen. Sollten während der Durchführung dieser Anleitung Fragen oder Probleme auftreten, so würde ich empfehlen, diese als Kommentar unter den Beitrag zu schreiben, damit Fehler vermieden werden können und andere Leser ebenfalls von der Antwort profitieren können.

Befindet sich Windows auf einer eigenständigen Festplatte, so können die nächsten Schritte übersprungen werden und direkt bei Schritt 5 vortgefahren werden. Ist Windows jedoch (genau so wie in meinem Fall) nur auf einer eigenständigen Partition installiert, so muss ein höherer Aufwand betrieben werden, um eine virtuelle Festplatte zu basteln, die Windows für die Reale hält. Bevor mit den Befehlen weiter unten loslegt werden kann, müssen zunächst ein paar Informationen notiert werden. Bei mir ist Windows 7 und Linux auf /dev/sda installiert und wir benötigen nun die Partitionstabelle von diesem Laufwerk:

[code=bash]
sudo fdisk -l -u /dev/sda
 
Disk /dev/sda: 1000.2 GB, 1000204886016 bytes
255 Köpfe, 63 Sektoren/Spur, 121601 Zylinder, zusammen 1953525168 Sektoren
Einheiten = Sektoren von 1 × 512 = 512 Bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Festplattenidentifikation: 0xdee1dee1
 
   Gerät  boot.     Anfang        Ende     Blöcke   Id  System
/dev/sda1   *        2048      206847      102400    7  HPFS/NTFS/exFAT
/dev/sda2          206848   204802047   102297600    7  HPFS/NTFS/exFAT
/dev/sda3      1937899518  1953523711     7812097    5  Erweiterte
/dev/sda4       204802048  1937897471   866547712   83  Linux
/dev/sda5      1937899520  1953523711     7812096   82  Linux Swap / Solaris
 
Partitionstabelleneinträge sind nicht in Platten-Reihenfolge
[/code]

Wie wir nun in der Ausgabe sehen, gibt es zwei Partitionen für Windows 7 /dev/sda1 und /dev/sda2. Die Erste ist nur knapp 100 MB groß und beinhaltet den Bootloader und das “Starthilfe”-System. Um Windows erfolgreich booten zu können benötigten wir beide Partitionen in unserer virtuellen Maschine. In meinem Fall liegen die Partitionen am Anfang der Festplatte, was im nächsten Schritt sehr hilfreich sein wird. Nun muss eine neue virtuelle Festplatte erzeugt werden, welche die beide Partitionen beinhaltet. Bevor wir aber wirklich beginnen, sollte eine Windows 7 DVD, entweder real oder als iso, bereit liegen, da diese in den nächsten Schritten benötigten wird.

[section]Erstellung der vituellen Festplatte mit realen Partitionen[/section]

In der Theorie müssen nur beide Partitionen an die VM weitergeleiten werden, damit die VM diese booten kann. Dies funktioniert aber aus zwei Gründen nicht, und zwar fehlt erstens ein gültiger Master Boot Record und zweitens hat Windows sich genau gespeichert wo seine Partitionen liegen. Der Trick besteht nun darin eine virtuelle Festplatte zu erstellen, die beide Partitionen beinhaltet, einen gültigen MBR + Partitionstabelle besitzt und diese an Virtualbox weiter zu geben.

Wie wir in Schritt 1 gesehen haben, sind die ersten 2048 Sektoren ungenutzt (0-2047) und erst ab 2048 beginnt die Windows Boot Partition. Diese Lücke müssen wir nun auffüllen. Da wir nicht einfach die ersten 2048 Sektoren übernehmen können (Diese beinhalten den realen MBR so wie den Grub Bootloader), muss ein Image erstellt werden, welches die Lücke füllt:

[code=bash]dd if=/dev/zero of=boot.mbr count=2048[/code]

count entspricht hierbei der Anzahl der Sektoren die aufgefüllt werden müssen und ist daher von der Ausgabe aus Schritt 1 abhängig. Desweiteren muss dd ggf. auch explizit die Größe eines Sektors mitgeteilt werden (bs=…), in meinem Fall entspricht die Größe eines Sektors 512 Byte (siehe Ausgabe von fdisk), was wiederum mit dem Standard von dd übereinstimmt, der Parameter kann daher weggelassen werden. Falls ihr mehrere GB an Daten auffüllen müsst, würde ich empfehlen nur eine Sparsefile anzulegen, statt alles mit 0 zu füllen. Diese Datei beinhaltet dann nur die Information wie groß sie ist und das alles mit 0en gefüllt ist, was zu einer erheblichen Speicherplatzeinsparung führt. Sie könnte folgendermaßen angelegt werden:

[code=bash]
dd if=/dev/zero of=boot.mbr count=0 seek=2048
[/code]

Diese Sparsefiles würden normalerweise zu starken Performanceverlusten führen, falls plötzlich Daten an beliebige Stellen innerhalb der Datei geschrieben werden sollen, da aber nur die ersten 512 Bytes von uns benutzt werden, stellt dies kein Problem da.

Nun muss die virtuelle Festplatte nur noch zusammengefügt werden und dies kann am einfachsten mit einem Software RAID gemacht werden. Dafür bietet sich der Linear-RAID-Modus an (NRAID), der einfach nur Festplatten hintereinander hängt. Zunächst muss die dazu benötigte Software installiert, und ggf. das zugehörige Kernel-Modul geladen werden.

[code=bash]
sudo apt-get install mdadm
sudo modprobe linear
[/code]

Nun müssen wir aus unserer Image Datei, welche unter anderem den MBR enthält, ebenfalls ein Device machen und dieses mit den Devices der Windows Partionen zusammenfügen:

[code=bash]
sudo losetup /dev/loop0 boot.mbr
sudo mdadm --build /dev/md0 --level=linear --raid-devices=3 /dev/loop0 /dev/sda1 /dev/sda2
[/code]

Die virtuelle Festplatte ist nun erstellt. Als nächsten Schritt muss deren Partitionstabelle, welche momentan nur aus 0en besteht, mit etwas sinnvollen gefüllt werden.

Zum Anlegen der Partitionstabelle nutzen wir wieder fdisk:

[code=bash]
sudo fdisk /dev/md0
[/code]

Falls die verwendete Sektorgröße nicht 512 Byte entspricht sollte diese mit dem Parameter -b an fdisk übergeben werden. Dieses mal befinden wir uns im interaktiven Modus von fdisk und müssen die Partitionstabelle von weiter oben nachbauen (allerdings nur die beiden Windows Partitionen!).

Hierzu legt ihr mit n neue Partitionen an, mit a könnt ihr sie als bootfähig makieren, mit t den Dateisystemtyp ändern (für Windows/NTFS ist der Typ 7), mit p das ganze nochmal überprüfen und zum Schluss mit w die Änderungen schreiben und das Programm beenden.

Ihr solltet dabei am besten die Partitionen genau in der Reinfolge anlegen, wie sie oben auch aufgelistet wurden. Es ist wirklich sehr wichtig, das hier jede Zahl identisch ist mit den Zahlen von Schritt 1, da sonst sehr schnell das NTFS Dateisystem zerstört werden kann. Für meinen Fall sieht das Endergebnis dann folgender Maßen aus:

[code=bash]
  Gerät  boot.     Anfang        Ende     Blöcke   Id  System
/dev/md0p1   *        2048      206847      102400    7  HPFS/NTFS/exFAT
/dev/md0p2          206848   204802047   102297600    7  HPFS/NTFS/exFAT
[/code]

Die Korrektheit der Partitionstabelle kann überprüft werden, indem die nun erzeugten Partitionen /dev/md0p1 bzw. /dev/md0p2 versuchsweise unter Linux gemountet werden (am besten read only!). Wenn sich beide problemlos lesen lassen, so ist die Wahrscheinlichkeit das die neue Partitionstabelle intakt ist recht groß. Anschließend müssen beide Partitionen aber auf jeden Fall wieder [u][b]ungemountet[/b][/u] werden. Gleiches gilt auch für die Originalpartitionen /dev/sda1 und /dev/sda2! Linux wird einen zwar nicht daran hindern diese zu mounten, auch wenn diese gleichzeitig in einer VM benutzt werden, aber die Chance das Dateisystem dabei nicht zu zerstören ist ungefähr so hoch wie im Lotto zu gewinnen.

[section]Virtuelle Festplatte in VM nutzen[/section]

Da die weiteren Schritte innerhalb der Virtuellen Umgebung durchgeführt werden müssen, unterscheiden sich diese je nach verwendeter Virtualisierungssoftware minimal. In diesem Tutorial wird jeweils die Einrichtung für Virtualbox, KVM und VMWare Player genauer beschrieben.

[subsection]Virtualbox[/subsection]
Damit unsere virtuelle Festplatte auch in Virtualbox genutzt werden kann (Virtualbox muss dafür natürlich installiert sein: [code]sudo apt-get install virtualbox[/code]), müssen wir eine Pseudo-Festplattendatei anlegen, welche nur den Verweis auf die virtuelle Festplatte beinhaltet:
[code=bash]sudo VBoxManage internalcommands createrawvmdk -filename windows.vmdk -rawdisk /dev/md0[/code]

[subsection]KVM[/subsection]
KVM benötigt keine extra Datei. Es muss lediglich /dev/md0 als Festplatte mit dem Format raw angeben werden.

[subsection]VMWare Player[/subsection]
Zuerst musst mit dem Wizard eine neue VM erstellt und dabei leider auch eine virtuelle Festplatte anlegt werden, die eigentlich nicht benötig wird (Diese kann aber später gelöscht werden). Nach dem die VM erzeugt ist, muss diese editiert werden: Entfernt die virtuelle Festplatte und fügt eine neue physikalische hinzu. Dazu muss beim Hinzufügen “Use a physical disk” ausgewählt werden. Nun muss /dev/md0 als Device angegeben und zum Schluss noch “Use entire disk” ausgewählt werden.

[section]Installation des Master Boot Records[/section]

Als nächster Schritt muss noch einen Bootloader in den MBR installiert werden. Es wäre möglich wie bei Linux GRUB zu verwenden, dieser benötigten aber eine Partition wo er seine Konfigurationsdateien etc. ablegen kann und dies würden wir noch weitere Partitionen benötigten. Aus diesem Grund eignet sich am besten der Original-Bootloader von Windows 7. Dieser besitzt bereits eine Partition und nur der fehlende MBR muss neu geschrieben werden. Dies lässt sich am einfachsten direkt innerhalb der VM erledigen.

Damit Virtualbox/KVM/VMWare überhaupt auf unsere Festplatte zugreifen kann, muss der aktuelle Benutzer entweder Rechte an der VMDK Datei (bei Virtualbox) und /dev/md0 besitzen ([code]chown user:group /dev/md0[/code]), oder die Software muss mit root-Rechten gestartet werden.

Falls noch nicht geschehen muss nun eine VM in Virtualbox/KVM angelegt werden. Bei Virtualbox entspricht dies dem selben Vorgehen wie beim Erstellen jeder anderen Window 7 VM, mit dem Unterschied, dass als Festplatte die windows.vmdk ausgewählt werden muss. Die KVM Nutzer können nun dasselbe mit Hilfe der Command Line oder dem virt-manager konfigurieren. Als CD-Rom muss nun entweder euer reales DVD-Laufwerk, in dem sich die Windows 7 DVD befindet oder eine äquivalente iso Datei ausgewählt werden. 

Nun starten wir die VM und drücken eine Taste, sobald uns die DVD dazu auffordert. Nach einigen Sekunden sollte dann der Sprachauswahldialog kommen. Als echte Linux Nutzer wollen aber lieber in der Konsole arbeiten und öffnen diese direkt mit Shift+F10 ;-).

Den MBR können wir nun einfach installieren in dem wir folgendes ausführen:

[code=bat]
bootrec /FixMbr
[/code]

Nun könnte man meinen das wir fertig sind, Windows hat in dieser Hinsicht aber eine Überraschung für uns: Der Bootloader sowie das System selbst identifiziert seine Partition anhand einer Nummer (4 Byte) innerhalb des Bootsektors und der Startadresse der Partition. Während die Startadresse der Partition dank unseres Vorgehens identisch sein sollte, macht uns ersteres Probleme. Damit wir unser Windows innerhalb der VM wie auch real booten können, müssen wir diesen Wert in unserem virtuellen MBR abändern. Wie auf [url=http://blogs.technet.com/b/markrussinovich/archive/2011/11/08/3463572.aspx]TechNet[/url] beschrieben ist, lässt sich das ganze durch zwei Methoden lösen: entweder mit dem Hex Editor oder mit ein paar Befehlen in der Recovery Umgebung. Da wir ja gerade schon mal in dieser Umgebung sind, habe ich mich auch gleich für die zweite Lösung entschieden.

Als erstes wird der Wert benötigt, den Windows erwartet. Um diesen zu erhalten müssen ein paar Registry Einträge geladen werden. Dazu muss man zunächst einmal wissen, welchen Laufwerksbuchstaben unsere Windows Partition zugewiesen bekommen hat. Da wir nur zwei Partitionen haben, schauen wir uns einfach mal deren Inhalt an:

[code=bat]
dir C:
dir D:
[/code]

Eine sollte leer sein, da alle Einträge versteckt sind und die andere sollten die gewohnten Ordner wie Windows, Users etc. beinhalten.

Nun müssen die passenden Registry Einträge geladen (wie [url="http://chentiangemalc.wordpress.com/2011/01/26/resolving-windows-7-startup-bsod-in-vmware-using-windbg/"]hier[/url] unter anderem beschrieben wird) und regedit gestartet werden (bei mir war D: die Windows Partition)

[code=bat]
reg load HKLM\Computer_System D:\Windows\system32\config\system
regedit
[/code]

Nun geht zu [code]HKEY_LOCAL_MACHINE\Computer_System\MountedDevices[/code] und es sollte eine Auflistung mit euren Laufwerken angezeigt werden. Für uns sind nun die ersten 4 Byte (bzw 8 Zeichen) die bei dem Windows Laufwerk (meistens [code]\DosDevices\C:[/code]) stehen interessant. Diese bilden genau die ID die wir benötigen.

[center]
[aimg="regedit.png" width=70%]Disk Signatures[/aimg]
[/center]

Nun ist die Darstellung zwischen der Registry und dem Befehl zum Setzen des Wertes nicht ganz konsistent und wir müssen die Bytes zwischen Little Endian und Big Endian swappen. Steht in der Registry der Wert [code]ab cd ef gh[/code] so muss nun den Wert [code]gh ef cd ab[/code] gebildet und mit Diskpart gesetzt werden.
Dazu müssen wir diskpart starten, die passende Festplatte auswählen (es sollte eigentlich nur eine in der VM vorhanden sein) und den Wert setzen:

[code=bat]
diskpart
DISKPART> select disk 0
DISKPART> uniqueid disk id=ghefcdab
DISKPART> exit
[/code]

Wir werden nacher ein Problem damit bekommen das Windows beim Booten ggf. nicht alle nötigen Treiber zum Zugriff auf die Festplatte lädt. Möchte man in KVM den IDE oder in Virtualbox einen PIIX3/PIIX4 IDE Controller benutzen oder VMware Player als Software verwenden, so sollten folgende Einträge in der Registry:

[code=bat]
HKEY_LOCAL_MACHINE\Computer_System\ControlSet001\services\atapi\Start
HKEY_LOCAL_MACHINE\Computer_System\ControlSet001\services\intelide\Start
HKEY_LOCAL_MACHINE\Computer_System\ControlSet001\services\pciide\Start
[/code]

auf 0 gesetzt sein. Dieses Problem ensteht dadurch, das Windows keine IDE Treiber beim booten lädt, wenn es vorher auf eine SATA Festplatte installiert wurde. Dies scheint im ersten Moment eine gute Idee sein, da so der Bootvorgang beschleunigt werden kann und normalerweise die Festplatte nicht plötzlich ihren Typ ändert, jedoch könnte dies auch für Backups problematisch werden.

Das war es auch schon. Nun müssen wir nur noch die Registry entladen (regedit muss vorher beendet werden, sonst wird ein Zugriff verweigert Fehler angezeigt):

[code=bat]
reg unload HKLM\Computer_System
[/code]

Alle Änderungen die wir durchgeführt haben betrafen nur unser Image des MBR und die reale Partitionen waren bis auf das Öffnen der Registry (und ggf. die Aktivierung zusätzlicher Treiber) noch unberührt. Nun können wir aber endlich Windows 7 nutzen.

[section]Windows 7 in der VM starten und nutzen[/section]

Ist Windows auf einer eigenen Festplatte installiert oder wurden die Schritte bis hier befolgt, so müssen wir nur noch die Einstellungen der VM etwas anpassen, bevor Windows endgültig gestartet werden kann. Als Erstes muss natürlich die (virtuelle) Festplatte eingebunden sein (Zum Laden einer Festplatte in Virtualbox/KVM siehe Schritt 3 – erster Befehl). Als Nächstes müssen wir noch die CPU Einstellungen etwas anpassen, da Windows spezielle Kernels für CPUs mit verschiedenen Fähigkeiten besitzt und nur den passenden installiert. Es kommt daher zu Problemen wenn dieser plötzlich gedowngraded wurde.

[subsection]Virtualbox[/subsection]
Unter System -> Beschleunigung sollte auf jeden Fall VT-X/AMD-V aktiviert sein. Desweiteren sollte auch ein Haken bei System -> Prozessor PAE/NX gesetzt sein, da dies eins der Kriterien ist, wonach die Kernel unterschieden werden. Bei NX handelt es sich um ein Sicherheitsfeature und sollte daher sowieso nicht deaktiviert sein.

[subsection]KVM[/subsection]
Falls ihr den virt-manager benutzt, solltet ihr bei den Einstellungen der Windows VM unter Processor auf den Button “Host-CPU-Konfiguration kopieren” klicken und speichern.
Nutzer der Command Line erreichen das selbe mit dem Parameter -cpu host. Weitere Performance Tipps erhält man auf der [url="http://www.linux-kvm.org/page/Tuning_KVM"]Webseite[/url] von KVM.

[subsection]VMware Player[/subsection]
Die Standardeinstellungen sind bereits optimal, wenn ihr Windows 7 (x64) als Betriebssystem ausgewählt habt. Nur der Sound muss ggf. extra aktiviert werden.

Wenn wir nun Windows 7 starten und in Schritt 4 keine extra Treiber aktiviert haben wird uns mit sehr hoher Wahrscheinlichkeit folgender Bluescreen begrüßen:

[center]
[aimg="bluescreen_vm.png"]Error: INACCESSIBLE_BOOT_DEVICE[/aimg]
[/center]

Die kryptische Nachricht 0x0000007B bedeutet INACCESSIBLE_BOOT_DEVICE und möchte uns mitteilen, dass zwar der Bootloader in der Lage war die Partition zu finden und den Kernel zu laden, aber der Kernel keine Ahnung hat, wie er auf seine Dateien zugreifen soll. Dies hat damit zu tun das Windows in der realen Umgebung auf einer IDE/SATA Festplatte mit bestimmten Chipsatz (IDE-Controller / SATA-Controller) installiert ist und Windows aus Geschwindigkeitsgründen darauf verzichtet die Treiber für andere Chipsätze zu laden.

[section]Ergebnis[/section]

[subsection]Virtualbox[/subsection]
Da Virtualbox eine ganze Reihe von SATA und IDE Chipsätzen unterstützt, ist es am einfachsten diese durch zu probieren, wenn man keine Änderungen in der Registry vornehmen möchte.

Bei mir klappte es zuerst als ich die Festplatte mit IDE und Chipsatz ICH6 eingebunden hatte (die anderen Chipsätze sollten nach dem Aktivieren der Treiber in der Registry auch funktionieren). Leider frierte das System mit diesem Chipsatz bei mir immer wieder für einige Sekunden ein. Da ich das ganze sowieso lieber per SATA einbinden wollte, habe ich es als nächstes mit SATA versucht und bekam wieder einen Bluescreen.

Als ich dann aber den IDE Controller mit dem virtuellen CD Laufwerk entfernt hatte und beides per SATA angebunden war, startete Windows 7 plötzlich ohne Probleme durch.

Ihr könnt nun die Guest Additions installieren. Da diese Treiber nur geladen werden, wenn die passenden (virtuellen) Geräte vorhanden sind, hat dies keine Auswirkungen auf das Starten in der realen Umgebung. Die einzige Ausnahme stellt die klassische Direct3D Beschleunigung da. Hier werden die DirectX 8/9 DLLs von Wine genutzt um die Direct3D Befehle auf OpenGL zu übersetzen und diese dann an den Host weiter zu reichen. Dies wirkt sich nachteilig auf das normal gebootete System aus (da die Umsetzung von DirectX auf OpenGL nicht perfekt funktioniert und unnötige Rechenleistung verursacht) und man sollte daher nur den neuen WDM Treiber installieren, der diese DLLs (zumindestens bei mir) nicht mehr ersetzt. Ein Test mit einem aufwendigen 3D Spiel hat nach der Installation der Guest Editions in der realen Umgebung keine merkliche Leistungsverschlechterung gezeigt.

Zum Abschluss noch ein Screenshot als Beweis, dass auch tatsächlich alles funktioniert! :-)

[center]
[aimg=vm-7.png width=50%]Windows 7 in Virtualbox[/aimg]
[/center]

[subsection]KVM[/subsection]
Möchte man die IDE Treiber in KVM nutzen, geht kein Weg daran vorbei die Änderungen an der Registry aus Schritt 4 durchzuführen (außer man hat den selben Chipsatz in seinem Rechner verbaut und Windows lädt den Treiber daher immer). Für die Performance wäre es jedoch besser direkt den Virtio Treiber zu benutzen, dieser müsste im Vorraus in Windows installiert werden. Da ich jedoch nicht noch mehr Treiber für virtuelle Maschinen installieren wollte und die anderen Kandidaten einige Vorteile wie 3D Beschleunigung anbieten, habe ich auf diesen Test verzichtet. Ein Test mit dem IDE Chipsatz brachte jedoch ein voll funktionsfähiges, wenn auch etwas langsames, Windows 7 zu Tage.


[subsection]VMware Player[/subsection]
Die physikalische Festplatte wurde bei mir als IDE Festplatte hinzugefügt, weshalb die IDE Treiber auf jeden Fall aktiviert sein müssen und dies ggf. manuell, wie in Schritt 4 beschrieben, nachgeholt werden muss. Insgesamt lief Windows in der VM sehr flüssig und war sogar in der Lage PlayReady DRM geschützte Videos ohne Probleme wieder zu geben (Es besteht bei DRM jedoch ein ähnliches Problem wie bei der Aktivierung, da die VM und das real gebootete System als zwei verschiedene Geräte erkannt werden). Die hierfür benötigte 3D Beschleunigung erhält man durch die Installation der VMware Tools.

Wenn die Anleitung bei euch funktioniert hat oder ihr Probleme hattet, so hinterlasst mir doch einfach einen Kommentar. Sollte jemand im Besitz einer ähnlichen Anleitung für das Booten einer Linux Partition unter Windows sein, so würde ich mich über eine Nachricht freuen. Gleiches gilt natürlich auch wenn ihr mit Hilfe meiner Anleitung und einer anderen Virtualisierungssoftware Windows 7 gestartet bekommt. Ich würde dann die passenden Befehle bzw. Hinweise ergänzen.
