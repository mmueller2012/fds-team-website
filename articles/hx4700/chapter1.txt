Title:			Linux on the hx4700: Chapter 1 – Minimal Debian
Tags:			Bluetooth, Debian, hx4700, Introduction, iPaq, IrDA, Wifi
Allow-Comments:	yes
Image:			hx4700-new.jpg
DateTime:		Thu, 20 Sep 2012 19:26:00 CET
UUID:			0ce06842d0fa6d0685f7d78c8ec756e6
Author:			Michael
URL:	articles/2012-09/linux-on-the-hx4700-chapter-1-minimal-debian.html

[info]
This series of blog entries gives an introduction how to install Debian on the hx4700 pda. We also prepared a ready to use debian Image, which you can get [aurl="debian-image.txt"]here[/aurl]. This project is still work in progress – so don't expect to work everything out of the box. Take a look at [aurl="chapter0.txt"]chapter 0[/aurl], which gives an overview about what is already working and the remaining problems. It might be possible that some parts of these instructions will change later, for example if it turns out, that some additional kernel features have to be enabled to circumvent these problems – if you want to stay up to date, just subscribe to this blog via RSS.
[/info]

[preview]

[center]
[tableofcontents]
[/center]

In this chapter I am going to explain how to install a minimal Debian system on the hx4700 pda. To achieve this, we need to compile a kernel, install a new bootloader and create a debian file system on a SD or CF card. At the end of this chapter, you will see a login shell on the display. The shell is kinda useless as we don’t have a keyboard to enter characters, but we will be able to control the system via USB. The installation of a graphical user interface and additional drivers will be explained in the [aurl="chapter2.txt"]next chapter[/aurl]. This chapter is more complex than the others, but most things can be done by just using copy & paste, so don’t be afraid ;-).

Before we are going to start, I have to inform you, that the original Windows installation will no longer be useable when following these instructions. Moreover I am using the SDG bootloader, which replaces the original bootloader. It is also possible to start Linux from within Windows by using the HaRET Bootloader. I cannot test this method easily as I removed the Windows bootloader, but you can find addiotional information in the comments (not necessarily in this chapter).
[/preview]

The prerequirements for this tutorial are simple:
[list]
[*] CF/SD card with at least 2 GB (for our Debian installation, recommended >= 4GB)
[*] computer with Linux (I used Ubuntu)
[*] CF card with at least 64 MB
[*] SD card with at least 256 MB
[*] SD card with at least 1 MB ;-)
[*] usb cradle/cable for the hx4700 
[/list]

You don’t need three SD or two CF cards, you can reuse them, but then you have to change the order of the instructions in this tutorial. An exception is the 1MB SD card, you can reuse the 256 MB card without changing the order by backing up the contents to your pc. In the following text I will first describe how to create a working debian system, before touching the hx4700 – to be able to install everything with just one SD and CF card you must change the order to: first install the Bootloader, compile & install kernel and then create the debian file system.

[section]General Notes[/section]
I am always appending -j4 to the command line of make to compile 4 files at the same time. You should change this to the number of cores your cpu has.

[section]Cross compiler[/section]
Before we can compile our own kernel, we need a cross compiler. You may get one from your favorite linux distrubtion or compile it on your own. I decided to compile a minimal cross compiler without a C library, which is just capable of compiling a kernel as this is sufficient for our purpose. First we create a working directory and one for our final cross compiler:
[code=bash]
mkdir cross
cd cross
sudo mkdir /usr/cross
[/code]

[subsection]Binutils[/subsection]
We need to compile the Binutils (ar, ld, …) before we can compile the gcc:

[code=bash]
wget http://ftp.gnu.org/gnu/binutils/binutils-2.22.tar.gz
tar -xf binutils-2.22.tar.gz
cd binutils-2.22
./configure --target=arm-linux-gnueabi --prefix=/usr/cross
make -j4
sudo make install
[/code]

Now you should see some arm-linux-gnueabi-* files in /usr/cross/bin.

[subsection]GCC[/subsection]
As I said before, we are going to compile a minimal gcc, so we don’t need to download the glibc library, but we still need GMP 4.2+, MPFR 2.3.1+ and MPC 0.8.0+. If you use a debian based distribution, you can simply install the required files by using:

[code=bash]
sudo apt-get install libgmp-dev libmpfr-dev libmpc-dev
[/code]

Now we are ready to compile our cross compiler:

[code=bash]
cd ..
wget ftp://ftp.gwdg.de/pub/misc/gcc/releases/gcc-4.7.1/gcc-4.7.1.tar.bz2
tar -xf gcc-4.7.1.tar.bz2
cd gcc-4.7.1/
mkdir obj
cd obj
../configure --target=arm-linux-gnueabi --enable-languages=c --disable-threads --prefix=/usr/cross
make all-gcc -j4
sudo make install-gcc
[/code]

Check if everything works by executing
[code=bash]
/usr/cross/bin/arm-linux-gnueabi-gcc --help
[/code]

You can delete all files in the working directory if you want to save space as we don’t need them any more.

[section]Kernel[/section]
We are going to use a 3.5.2 Kernel on the hx4700. You may try to use a newer version or a different configuration but don’t expect it to work. Some options cause an immediate crash and you need a special rs232 cable to get any output from the device as the screen will not show any error messages.

First we need to download and extract the kernel:

[code=bash]
cd ../..
mkdir kernel
cd kernel
wget http://www.kernel.org/pub/linux/kernel/v3.0/linux-3.5.2.tar.bz2
tar -xf linux-3.5.2.tar.bz2
[/code]

There are some minor bugs in the Kernel which should be patched:

[subsection]Rotated Display[/subsection]
If you want the possibility to rotate the framebuffer, you should apply a patch by Roman Elshin (see [url="http://comments.gmane.org/gmane.comp.handhelds.ipaq.hx4700.linux/1876"]here[/url]), which didn’t find its way to the kernel.
Open the file arch/arm/mach-pxa/hx4700.c in the kernel directory and search for:

[code=cpp firstline=472]
.pixclk_divider_rotated = 4
[/code]
[code=cpp firstline=494]
.pixclk_divider_rotated = 4
[/code]

Now replace the 4 with a 9. This corrects the pixel clock divisor when you use the framebuffer in landscape mode. It would also be possible to tell the X server to rotate the screen, but this would cause extra cpu usage.

[subsection]Bluetooth[/subsection]
If you want to use bluetooth, you should apply an updated version of [url="http://bugs.openembedded.net/show_bug.cgi?id=4959"]this[/url] patch by Oliver Winker:

[code=bash]
wget http://fds-team.de/cms/downloads/hx4700/bluetooth.patch
cd linux-3.5.2
patch -p1  < ../bluetooth.patch
[/code]

To be finally able to use Bluetooth, the kernel option for the GPIO /proc/sys/ interface has to be activated. You need to enable [code]Device Drivers --> GPIO Support --> /sys/class/gpio/... (sysfs interface)[/code] manually by using [code]make menuconfig ARCH=arm[/code] as the option is disabled in my provided .config file. This allows us to toggle the power supply for the Bluetooth chip from user space. We need the described solution because there is no dedicated kernel driver for the hx4700 device which does this job.

[subsection]CPU Speed[/subsection]
If you want to use the full CPU speed, you should pass “pxa27x_maxfreq=624″ as command line to the kernel. As this didn’t work for me, I patched the kernel to set 624 Mhz as default maximum. Open arch/arm/mach-pxa/cpufreq-pxa2xx.c and search for:

[code=cpp firstline=236]
pxa27x_maxfreq = 416000;
[/code]

Replace 416000 with 624000 and save.

[subsection]Compile Kernel[/subsection]
I tried to create a kernel config which enables all features of the hx4700 (except Bluetooth, see above!), so you can just use my config, if you don’t need a special configuration.
[b][UPDATE 2.10.2012]:[/b] added missing VCC Core Regulator for PXA27X CPUs

To get my configuration (still work in progress!):

[code=bash]
wget http://fds-team.de/cms/downloads/hx4700/.config
[/code]

If you want to alter the config anyway, you can use
[code=bash]
make menuconfig ARCH=arm
[/code]

The next step is to compile the kernel and the modules:

[code=bash]
export PATH=$PATH:/usr/cross/bin/
make zImage ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- -j4
make modules ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- -j4
[/code]

The kernel is now ready to use and we can continue with debian.

[section]Minimal Debian Installation[/section]

There are several possibilities to get a working minimal Debian system, but most of them are complicated and I tried to find an easy solution. I use debootstrap to get an initial file system, but this is only simple if your processor architecture matches the target architecture. This was no problem for me as I have an ARM based Netbook with Ubuntu, but to give everyone the possibility to follow this guide, I decided to use qemu. Before we can use debootstrap, we have to format our CF/SD card and install the kernel modules first:

[subsection][anchor=formatcard]Format SD/CF card[/subsection]
Grab your SD/CF card, which should be used for the final file system and put it into your card reader. If your filemanager automatically mounts the card, you should unmount it again. Take a look at your kernel log with dmesg and find out the device name of the card. In my case it was /dev/sde. If you see this entry:

[code=bash]
[15502.278892] sde: detected capacity change from 7969177600 to 0
[/code]

than you most probably used your filemanager to unmount it. Some filemanagers do not only unmount the device, but also tell the kernel to remove it completely. To solve the problem, reinsert your card and unmount the device in your terminal with umount.

Now we can start to format the device:

[code=bash]
sudo fdisk /dev/sde
[/code]

This opens a new menu. Write p and press [ENTER] to see all available partitions. Now use d to delete a partition and repeat it until all partitions are gone. The next step is to create two partitions: one partition for our debian installation and one swap partition. Im my case I used a 8 GB sd card and used 7 GB for my file system. To create the file system partition, press n and answer all questions except the question for the Last Sector by pressing [ENTER]. When fdisk asks you for the last sector enter +7GB (change it depending on the size of your SD card). Now change the file system type to Linux by entering t and write 83 as Hex Code. For the swap partition we enter n again and answer all questions with [ENTER]. Change the file system type to 82 (Linux Swap / Solaris). Your partition table should be similar to mine (Press p and [ENTER] to display it):

[code=bash]
   Gerät  boot.     Anfang        Ende     Blöcke   Id  System
/dev/sde1            2048    14682111     7340032   83  Linux
/dev/sde2        14682112    15564799      441344   82  Linux Swap / Solaris
[/code]

Press w and [ENTER] to write your changes to the disk. You may need to reinsert your card to notify the kernel about the new partition table.

We now need to format our partitions to use them:
[code=bash]
sudo mkfs.ext3 /dev/sde1
sudo mkswap /dev/sde2
[/code]

If you want to use a different file system than ext3, you may need to change the kernel config. I recommend to use a file system with journaling support, as you may not always have the possibility to shutdown the device properly.

[subsection]Install Kernel modules[/subsection]
Now you have to mount the ext3 partition again. Go back into your kernel directory, if you left it, and execute:

[code=bash]
sudo make modules_install ARCH=arm INSTALL_MOD_PATH=MOUNTDIR
[/code]

Replace MOUNTDIR with the directory where you mounted the ext3 partition.
You should now be able to see files in MOUNTDIR/lib/modules/3.5.2/.
Unmount the partition again.

[subsection]Prepare Qemu and install base system[/subsection]

As I said before, you don’t need to use Qemu if you have a native ARM machine, Qemu is only a fallback method. First of all you need to install qemu-system-arm, which is not always included in the regular qemu package. In Ubuntu you can install it by executing:

[code=bash]
sudo apt-get install qemu-kvm-extras
[/code]

Now we create a directory for all needed files:

[code=bash]
cd ../..
mkdir qemu
cd qemu
[/code]

To keep everything simple we just use a prepared arm debian image:

[code=bash]
wget http://people.debian.org/~aurel32/qemu/armel/initrd.img-2.6.32-5-versatile
wget http://people.debian.org/~aurel32/qemu/armel/vmlinuz-2.6.32-5-versatile
wget http://people.debian.org/~aurel32/qemu/armel/debian_squeeze_armel_standard.qcow2
[/code]

You can get more information about this image [url="http://people.debian.org/~aurel32/qemu/armel/"]here[/url]. The only relevant information for us is that the root password is “root”. To start our virtual system, we need to type:

[code=bash]
sudo qemu-system-arm -m 512 -nographic -M versatilepb -kernel vmlinuz-2.6.32-5-versatile -initrd initrd.img-2.6.32-5-versatile -append "root=/dev/sda1 console=ttyAMA0" -hda debian_squeeze_armel_standard.qcow2 -hdb /dev/sde
[/code]

Don’t forget to replace /dev/sde with the device of your SD card. I disabled the graphic mode “-nographic” so that you can directly copy & paste the commands into the console of the virtual system. This should make it easier to follow the tutorial ;-)

When you see the login shell, use root:root to login into the system and install debootstrap (in qemu):

[code=bash]
apt-get update
apt-get install debootstrap
[/code]

It's time to install the base system (in qemu):

[code=bash]
mkdir ipaq
mount /dev/sdb1 ipaq
debootstrap --arch armel wheezy ipaq http://ftp.de.debian.org/debian
[/code]

This takes some time, so you can take a break ;-).
The program should exit with

[code=bash]
I: Base system installed successfully.
[/code]

and we can start the configuration of our final debian system.

[subsection]Configuration of Debian[/subsection]

Before we can enter our new system via chroot, we should mount proc (in qemu):

[code=bash]
mount proc ipaq/proc -t proc
chroot ipaq
[/code]

The first thing we should do inside our system is to update the apt cache (in qemu):

[code=bash]
apt-get update
[/code]

The next step is to install locales support, a lightweight SSH Server and replace some stuff with a more lightweight implementation (in qemu):

[code=bash]
apt-get install locales busybox-syslogd mingetty dropbear htop
[/code]

You can safely ignore the “Can not write log, openpty() failed (/dev/pts not mounted?)” errors. The locales error messages can now be fixed by (in qemu):

[code=bash]
dpkg-reconfigure locales
[/code]

You have to select the locales to generate. I recommend to activate en_US.* and your native language. Do not enable all locales! It takes very long to generate them all and it will consume a lot of space. After this, you have to select your preferred language. I selected de_DE.UTF-8.

The timezone is also not configured yet, we can correct this by executing (in qemu):

[code=bash]
dpkg-reconfigure tzdata
[/code]

The next step is to edit /etc/fstab to use our swap partition. Open the file with nano (in qemu):

[code=bash]
nano /etc/fbstab
[/code]

paste:

[code=bash]
rootfs               /                    auto       defaults              1  1
/dev/mmcblk0p2       none                 swap       sw                    0  0
[/code]

Replace mmcblk0p2 with sda2 if you are using a compact flash card and pres [CTRL] + [X] to save the changes and exit the program (You have to acknowledge it by pressing [Y], [ENTER]).
To be able to access the device via USB, you should configure the network as a next step (in qemu):

[code=bash]
nano /etc/network/interfaces
[/code]

Scroll down by using the cursor and append:
[code=bash]
allow-hotplug usb0
iface usb0 inet static
    address 192.168.7.2
    netmask 255.255.255.0
    network 192.168.7.0
    gateway 192.168.7.1
[/code]

Save the file and exit nano.
Now we are going to save some ram. This is optional, but I would recommend it. Open inittab with nano (in qemu):

[code=bash]
nano /etc/inittab
[/code]

scroll down until you find the following block:

[code=bash]
1:2345:respawn:/sbin/getty 38400 tty1
2:23:respawn:/sbin/getty 38400 tty2
3:23:respawn:/sbin/getty 38400 tty3
4:23:respawn:/sbin/getty 38400 tty4
5:23:respawn:/sbin/getty 38400 tty5
6:23:respawn:/sbin/getty 38400 tty6
[/code]

and alter it to

[code=bash]
1:2345:respawn:/sbin/mingetty tty1
#2:23:respawn:/sbin/getty 38400 tty2
#3:23:respawn:/sbin/getty 38400 tty3
#4:23:respawn:/sbin/getty 38400 tty4
#5:23:respawn:/sbin/getty 38400 tty5
#6:23:respawn:/sbin/getty 38400 tty6
[/code]

We can just use one tty as we don’t have a keyboard to switch between them, so we can safely disable the others. The reason for using mingetty is that it's smaller than getty. Save and exit nano.

[info]Mingetty does not support serial connections. If you are going to debug your device using a special rs232 cable than you should leave getty enabled[/info]

We should change the hostname to something useful, because our system copied the hostname from the virtual machine. I used 'hx4700' as hostname, but you can replace it with what ever you want (in qemu).

[code=bash]
echo hx4700 > /etc/hostname
echo "127.0.0.1 hx4700" >> /etc/hosts
echo "::1   hx4700" >> /etc/hosts
[/code]

Our last step in the virtual machine is to change the root password and add a new user (in qemu):

[code=bash]
passwd
adduser USERNAME
[/code]

Now we can exit the chroot environment and shutdown the virtual machine (in qemu).

[code=bash]
exit
shutdown -hP now
[/code]

When you see “[ 6754.433061] System halted”, you can kill qemu or close the console.

Congratulations :-)! You did the most complicated part and you just need to install the bootloader before booting debian the first time.

[section]Bootloader[/section]

[warning]
Be careful when flashing your hx4700 and store your backups at a safe place! You can ask me if you have questions, but please don’t expect me to have a solution for every possible problem which can occur! If you decide to install Debian you have to do it on your own risk! Moreover, by following these steps, you will most probably lose your warranty (although I don't think anyone has still a warranty for such an old device ;-))!
[/warning]

Before we replace the bootloader, you should make a complete backup of your ROM, this includes the bootloader and windows.

[subsection]Backup ROM[/subsection]

Grab a SD card with at least 256 MB and backup it’s content as all data on the card will be overwritten. Remove all cards from the device and press Contacts + iTask while doing a reset. Connect the device to your computer via USB and you should see the hp logo with the text USB on the screen of your hx4700. It may be hard to see as the backlight is turned off. Take a look at your kernel log with dmesg and you should see the following message:

[code=bash]
usb 1-1.3: PocketPC PDA converter now attached to ttyUSB0
[/code]

We already have a virtual serial connection to the hx4700, but we need a terminal emulation program to communicate with the flash utlitiy. I use minicom:

[code=bash]
sudo apt-get install minicom
sudo minicom -s -o
[/code]

Select the Serial options and change the connection to /dev/ttyUSB0 (or whatever dmesg printed), disable Hardware Flow Control and quit the menu by pressing [ENTER]. Select Exit to start the terminal (this quits the menu and starts the terminal, so don’t be confused).
After pressing [ENTER] you should see

[code=bash]
USB>
[/code]

Insert the SD Card and execute d2s

[code=bash]
USB>d2s
[/code]

The flash utility will now backup the complete rom on the sd card and verify its content. If you get a CRC Error, this can be caused by a broken card. I always got CRC errors and tried three different SD cards. I noticed that one card always produced a totally broken image and the differences between the two others were just some bytes. I assume the flash utility does not only backup the rom, but also copies parts of the ram (the rom is accessible as ram, so there is no real difference for the cpu) which changes during the backup process, so I wouldn’t take this error too seriously.
I recommend to backup the content of the SD card to your computer. You can use dd to do so.

[subsection]Install Bootloader[/subsection]

Now grab the bootloader from the SDG website:

[code=bash]
wget http://sdgsystems.net/pub/ipaq/hx4700/starterkit/20060615-gpe/bootldr-1.2.4.rom
[/code]

We need to patch the kernel command line, otherwise the kernel cannot find the root file system. Open your favorite hex editor (I used ghex) and search for the following string:

[code=bash]
root=/dev/mtdblock2 rw rootfstype=jffs2 console=ttyS0,115200n8
[/code]

and replace it with:

SD Card:

[code=bash]
root=/dev/mmcblk0p1 rw console=tty0 rootdelay=2
[/code]

CF Card: [b][UPDATE: 2.12.2012]:[/b] it should be sda1 not hda1

[code=bash]
root=/dev/sda1 rw console=tty0 rootdelay=2
[/code]

It is very important that you do not change the size of the file! Overwrite all unused characters with white spaces (0x20) to keep the size. You may need to play around with the root delay option as this may depend on your card. I use rootdelay=1, but this does not always work with one of my SD cards.

Put a SD card into the card reader of your computer, which can be overwritten (you can reuse the card from the previous step if you backed up all the content). Unmount the file system if it was mounted automatically and find its device name with dmesg.
Write the bootloader to the SD card:

[code=bash]
dd if=bootldr-1.2.4.rom of=/dev/sde
[/code]

Now put the sd card into your hx4700 and reboot into the HP Flash utility (Reset + iTask + Contacts). The flash utility automatically detects the SD card and will ask you to press Power to start flashing. You can safely ignore the CRC Error. The bootloader is installed now and we can continue with the Kernel.

[subsection]Install Kernel[/subsection]

To be able to install the new kernel, we need to format the CF card. Use fdisk to create the following partition table:

[code=bash]
  Gerät  boot.     Anfang        Ende     Blöcke   Id  System
/dev/sdb1            2048      124927       61440    6  FAT16
[/code]

Just remove all partitions and add one with a size of 64 MB. Change the file system type to 6.
The next step is to create a FAT16 file system on the partition:

[code=bash]
sudo mkfs.vfat -f 16 /dev/sdb1
[/code]

Mount the file system and create a file called reflash.ctl with the following content:

[code=bash]
kernel-3.5.2 zImage - kernel
[/code]

Copy the zImage of your kernel (KERNEL_DIRECTORY/arch/arm/boot/zImage) on the CF card and unmount the file system.
Now put the the CF card inside your hx4700 and reboot it into the flash mode of your new SDG bootloader (Reset + Contacts + Mail, you can still access the hp flash utillity). The Bootloader should show up a Menu with just one entry “kernel-3.5.2″. Press iTask to select the entry and Record to start flashing the kernel. When everything is done, you will be prompted to press Power. Put the card with the Debian file system inside your device and boot into your new Debian system :-)

[section]First boot of debian[/section]

After restarting your System you should see the kernel messages scroll down on your display. After some seconds you should see the following screen:

[center]
[aimg="login_prompt.png"]Login prompt after boot[/aimg]
[/center]

The login prompt is useless for us as we don’t have a keyboard to enter our username and password, but we installed a SSH server in a previous step. Your linux distribution should now detect a new usb ethernet connection and you need to change the connection settings in your network manager or add the following configuration to /etc/network/interfaces:

[code=bash]
allow-hotplug usb0
iface usb0 inet static
    address 192.168.7.1
    netmask 255.255.255.0
    network 192.168.7.0
[/code]

It may be necessary to execute

[code=bash]
sudo ifdown usb0; sudo ifup usb0
[/code]

before you can use the connection. Now you should be able to login into the ssh server of your hx4700:

[code=bash]
ssh root@192.168.7.2
[/code]

It's now possible to execute some basic commands, but you cannot access the internet. We will introduce the necessary configuration in the next chapter.

As last part of this chapter, I want to show you the benefits of our work:

[center]
[aimg="htop_minimal.png"]htop[/aimg]
[/center]

As you can see on the screenshot, we are just using 5 MB ram, which is quite impressive for a Debian server running a SSH server and bash. We could also reduce this even more by using busybox instead of bash and the common gnu utilities, but this would get us into more trouble as not all debian scripts work flawless with busybox. There is a special port of Debian called [url="http://www.emdebian.org/"]Emdebian[/url] which uses busybox. This port is also a bit more optimized for embedded devices, but as it does not offer all packets from the regular debian repository I preferred to use method.

This should be enough for this chapter. If you have any questions or improvements feel free to write a comment.

Continue by reading [aurl="chapter2.txt"]chapter 2[/aurl].