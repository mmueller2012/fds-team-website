Title: 			Linux: Limit IPv6 connectivity to specific programs
Tags:			IPv4, IPv6, SixXS, AYIYA, Freenet6, TSP, network, namespace, gogoc, aiccu, SOCKS5, Proxy, netns, Chrome, firefox
Allow-Comments:	yes
Image:			firewall.svg
Show-in-Index: 	yes
DateTime:	Mon, 17 Jun 2013 01:49:01 CEST
UUID:	8c15e25957f4701fb56bd44b3ca048f7
URL:	articles/2013-06/linux-limit-ipv6-connectivity-to-specific-programs.html

[preview]
This article explains how to limit IPv6 access in Linux to only selected applications. You may ask yourself why you should bother about restricting IPv6 to some specific programs and do not use it systemwide as it is the future. The main problem is to secure an IPv6 network which is much more complicated than using a typical IPv4 network consisting of a router and several devices behind it. The idea of this article is to select only several applications which are allowed to use the new IP standard, so that you have much more control over it. This is also useful if you are using a tunnel broker and do not want to redirect all traffic (like for example email) through this tunnel. Even if you actually don't necessarily need an IPv6 address, it might be useful, if you need to get an IP address in a different country because of some sort of geo blocked service...[/preview]

Before we actually describe how to get IPv6 or how this separation works, we will at first take a look at the typical homenetwork. If you already have IPv6 (via tunnel or native) and just want to see how the separation works, you can immediately continue to read chapter 2.

[aimg width=25em float=left]simplenetwork.svg[/aimg]

The router is connected to your ISPs network and all your devices like computers or smartphones are connected to your router. When using IPv4, the router only gets one public IP and is using a Network Address Translation (NAT) mechanism to give internet access to all connected devices. Your local clients only get a so called private IP, for example 192.168.1.10 or 10.0.0.4, which is not reachable from the internet. 

Although it was not an intended behaviour, these NATs act as some kind of firewall, because they deny all incoming connections. The router simply does not know which device inside the network should get the connection request as all devices have the same public IP and therefore such requests are rejected (if no DMZ is configured). The router will only allow incoming packets if it was an answer to a request made from a local device. For this reason the area behind the router is called homenetwork as only devices inside your network should be able to communicate with each other. The problem is that most operating system and programs only have a few or completely no security checks for this network, because they assume that you do only use trusted devices inside your network.

When using IPv6 every single device gets it's own public IP address and there is no homenetwork any more. Most programs still assume that only devices inside your network are capable to connect, but this is wrong. If you for example were using a local ftp server without a password protection, this was not a big deal while using IPv4 as only devices inside your network were able to connect, but when using IPv6 everyone around the world can get your files. So it may be a good idea to restrict IPv6 connectivity to only some programs as this prevents you from opening any security holes. Needles to say it's possible to use some kind of firewall in Linux to secure IPv6, but this is more complicated.

Another reason for restricting access to IPv6 is when using a tunnel broker. You may not trust the broker and do not want all your data to go through this tunnel or you sometimes want to use the full speed of your connection for downloads or video streaming.

[b][color=gray][UPDATE 17.06.2013][/color][/b]

Due to the lot of feedback I decided here some additional explaination about the alternatives and in which situations this isn't an appropriate solution. Besides the completely positive reactions there are always two types of criticism:
[list]
[*] You simply should use IPv6 without a firewall, as otherwise it has no advantage over IPv4 and programs can still run into trouble when trying to establish direct connections.
[*] Most routers come with a basic firewall already dropping unrelated packets, so this is useless except when using a tunnel broker.
[/list]

As you might notice both arguments are contradicting each other - on the one side you want to be able to establish direct connections without being blocked by a firewall, but on the other side it might be useful to activate or install a firewall to prevent security problems.

I think the following quote from the [url="http://arstechnica.com"]arstechnica.com[/url]-blog summarizes this really good:
[quote="Iljitsch van Beijnum" url="http://arstechnica.com/gadgets/2007/05/ipv6-firewall-mixed-blessing"]
So, ironically, what's required to make IPv6 work through a stateful firewall is almost identical to what's required to make IPv4 work though NAT. This means the IETF's efforts to keep IPv6 NAT-free in order to make protocols do their job without messy workarounds are defeated by the notion that everything should be firewalled.
[/quote]

Let me first say something to the first statement above (not using a firwall): Running a system without firewall rules can be dangerous if the user doesn't periodically check whats going on and which programs have opened listening ports - imagine you install updates, and by accident an installed but disabled ftp server gets enabled again, without even noticing it. For a public reachable server which just hosts a limited number of services like a webserver, it might be okay not to use a firewall as changes on the system are rare and observed by an admin. Another problem might be the usage of old devices, probably already capable to talk IPv6, but still containing a lot of serious bugs, which probably will never be fixed.

The second point (router's containing a basic firewall) might be true for the majority of all routers, but this wasn't true for the first generation of IPv6 routers. The idea of these firewalls rules is to simulate the behaviour of NAT, which is against the idea of IPv6 to allow each device to establish a direct connection to each other.

It is not hard to see that you can not combine both ideas, so our solution is to give some applications full IPv6 access (no filtered ports) and all other programs simply cannot use IPv6. I think this fits most the idea of IPv6. The user can manually select which programs are started inside the IPv6 enabled namespace with full IPv6 support, so even peer-to-peer application should run without any communication issues. The additional latency overhead can be neglected compared to a usual setup. Additionally you can specify the access for each application individually (either by starting it in the namespace or using the SOCKS5 proxy), which wouldn't be possible with hardware based approaches. 

An improved idea of our mechanism would be not to deny IPv6 completely to all other applications, but using a firewall acting like a NAT for them. This would be easily possible by just changing some lines, but as I unfortunately don't have native IPv6 support and must use a tunnel, I also want to decide which data should go through it. Feel free to send us your changed scripts, if you think that they may be usefull for others.

[b][color=gray][/UPDATE][/color][/b]

[tableofcontents]

[section] Get IPv6 [/section]

The first necessary ingredient is of course the IPv6 support. If your ISP directly provides you with native IPv6 support, you can immediately continue with the next section. Otherwise you don't have to worry as there are a lot of tunnel broker services providing IPv6 via different methods. Each of these methods has its advantages and disadvantages - but for the less experienced users I would recommend to use one supporting the AYIYA or TSP protocol which also works behind a NAT (i.e. router). A big list of tunnel brokers can be found at [url="http://en.wikipedia.org/wiki/List_of_IPv6_tunnel_brokers"]Wikipedia[/url]. Of course I was not able to test each individual one, but in principle each of these should work the same way, when they are using the same method.

Please note that by default these methods (similar to native IPv6 support) are intended to add IPv6 to the whole system, not to just some specific programs. This requires some additional configurations.

[subsection]AYIYA (e.g. SixXS)[/subsection]

The AYIYA (anything in anything) protocol is one of the possible methods to use a tunnel broker behind a NAT. The most famous provider using this protocol is SixXS. You can easily get an account at [url=http://www.sixxs.net/]sixxs.net[/url] and then you need to request a tunnel which may take some time. You can only get an account from a Point of Presence (PoP) nearby you. While selecting the type of tunnel to use, choose AYIYA as it's most easy to use behind a router.

After completing all steps you just need to install their client. On a debian based system the installation of the client can be done by executing:

[code=bash]
sudo apt-get install aiccu
[/code]

Simply enter your username and password when asked and everything should work out of the box. Their client will immediately establish an IPv6 tunnel after the installation, so you should stop it as soon as poosible and prevent it from starting during boot by executing:

[code=bash]
/etc/init.d/aiccu stop
sudo update-rc.d -f aiccu remove 
[/code]

[subsection]TSP (e.g. Freenet6)[/subsection]

The Tunnel Setup Protocol (TSP) is very smiliar to AYIYA as it also support a method to punch through NATs. One tunnel broker which is using this mechanism is Freenet6. You can get a tunnel on their homepage [url=http://www.gogo6.com/]gogo6.com[/url] in just a couple of minutes. Just select Services -> Freenet6 and select Sign Up at the top of the page. To get a tunnel follow the tunnel setup process at Services -> Setup your Freenet6 account after creating a normal account. 

After acquiring a tunnel, you need to install the client. On a debian based system the installation of the client can be done by executing:

[code=bash]sudo apt-get --no-install-recommends install gogoc[/code]

The "--no-install-recommends" parameter prevents the system from installing the router advertising daemon. This daemon would advertise your computer as IPv6 router if you select a wrong parameter. As we don't want to provide the new IP standard to the complete network, we simply don't install the needed software. After installating the client, you should stop the client (if its running) and prevent it from starting during boot by executing:

[code=bash]
sudo /etc/init.d/gogoc stop
sudo update-rc.d -f gogoc remove
[/code]

Now you need to setup the client by editing [code]/etc/gogoc/gogoc.conf[/code] as a root user. Change the following values:
[code=text]
userid=YOUR_USERNAME
passwd=YOUR_PASSWORD

server=authenticated.freenet6.net

auth_method=any
[/code]

and save the file. The next step is to install the private keys of their severs by executing the client in foreground mode.

[code=bash]gogoc -n[/code]

When the client asks you to download the keys, accept it, wait a short moment and kill the client by pressing CTRL+C.


[subsection]6in4 / heartbeat (SixXS and many more)[/subsection]

For the more experienced users the 6in4 / heartbeat protocol is probably the best choice as it has the smallest overhead compared to the other methods. Unfortunately I didn't have the time to test it myself. The idea of this protocol is that the data is not embedded into a tcp or udp stream but instead a new protocol is defined and only the IPv4 header is added. Most routers will only drop this packets as they do not understand the protocol. If you have luck and you are using a linux based router with ssh access, you may be able to activate the tunnel mechanism on the router or you may be able to add an iptables rule to redirect protocol 41 to your computer and enable it on your local device.
If you want to share your experiences using this method feel free to write us a comment.

[section]Separating IPv6[/section]

[warning]You have to use a Linux kernel above or equal to version 3.0 to use network namespaces![/warning]

In order to ensure that the main system doesn't have IPv6 support we should first disable it. In the following text we assume that your network device is [code]eth0[/code]. You need to replace it if you are using a different device.

If you have native IPv6 support it is sufficient to disable it the following way:
[code=bash]
sudo su
echo "1" > /proc/sys/net/ipv6/conf/eth0/disable_ipv6
exit
[/code]

If you want to disable it permanently, you simply need to add the following entry to [code]/etc/sysctl.conf[/code]
[code=text]
net.ipv6.conf.eth0.disable_ipv6 = 1
[/code]

For AYIYA or TSP we already disabled the auto start mechanism, which should be sufficient.

Now we are going to setup a separate network namespace and add IPv6 support there - this ensures that there no services running which aren't intended to be available to the public.

The following diagram shows the way we will configure our network:
[center][aimg]namespaces.svg[/aimg][/center]

We will create a new so called network namespace. The idea of these namespaces (ns) is that network devices are only available in one namespace and only programs inside them can use such a device. They separate the complete network traffic, you even will have two loopback interfaces, one for every namespace. So sending packets to 127.0.0.1 inside the ns will not reach programs outside the namespace. 

We first create a virtual ethernet device (veth), which consits out of two logic ethernet devices (we name them tun6v0 and tun6v1). They act as a bidirectional pipe, all data send to tun6v1 will come out of tun6v0 and vice versa. This allows our namespaces to communicate with each other. Then we will move tun6v0 to our tunnel namespace, so that both namespaces can communicate with each other. The next step is to get IPv4 access inside our tunnel ns. To do this, we will create a network bridge over eth0 and tun6v1. The next step is to remove the IP configuration from eth0 and setup the IPv4 configuration on our network bridge br0. The tun6v0 device inside the ns will now be able to reach the router with its own virtual mac address, but it also need it's own IP address. To get an own IP for the device we will start an dhcp client inside the namespaces. At the end your computer will have 2 IPs assigned, one for each namespaces. This may be a bit overkill, but it represents the level of separation. If you are using a tunnel broker, you will also get a tun interface inside the tunnel namespace.

An IPv6 packet send inside your network would take the following way when using a tunnel broker:

[code=text]tun -> tun6v0 -> tun6v1 -> br0 -> eth0 -> router -> tunnel broker[/code]

To configure all this we need to install the bridge utilities:

[code=bash]
sudo apt-get install bridge-utils
[/code]

The configuration described above can be tested using the following command lines:

[code=bash]
# create new namespace
sudo ip netns add tunnel6

# create a new virtual ethernet device with 2 endpoints
# move one endpoint to the new namespace
sudo ip link add name tun6v0 type veth peer name tun6v1
sudo ip link set tun6v0 netns tunnel6

# create a new network bridge and attach 
# our real ethernet device and one of the virtual
# endpoints to the bridge
sudo brctl addbr br0
sudo brctl addif br0 eth0
sudo brctl addif br0 tun6v1

# remove ip address from our real interface, move it to br0
# and fire everything up
sudo ifconfig eth0 0.0.0.0 up
sudo ifconfig tun6v1 up
sudo ifconfig br0 dynamic up
sudo dhclient br0

# inside of the namespace enable all needed interfaces
# use dhcp so that our fake interface will get an extra
# ip address
sudo ip netns exec tunnel6 ifconfig lo up 
sudo ip netns exec tunnel6 ifconfig tun6v0 dynamic up 
sudo ip netns exec tunnel6 dhclient tun6v0
[/code]

After this you need to start your tunnel broker by executing:

[code=bash]
# SixXS
sudo ip netns exec tunnel6 /usr/sbin/aiccu start

# Freenet6
sudo ip netns exec tunnel6 /usr/sbin/gogoc
[/code]

and now you will have IPv6 access only inside your ns. Every programm started inside the ns will also have access to IPv6, but programs started outside (i.e. from your desktop menu or a normal terminal) can not reach the IPv6 internet. If you are using native IPv6, you may also need to disable it on br0.

[section]Allow IPv6 for specific programs[/section]

[subsection]The direct way[/subsection]

After you have configured IPv6 for the separate namespace called [code]tunnel6[/code], you can now allow any program to use it just by starting it using:

[code=bash]
sudo ip netns exec tunnel6 /your/program
[/code]

This program will now be able to use IPv6 and every opened port will also be reachable through your public IPv6 address. In case you want to start firefox inside your ns, you must execute:

[code=bash]
sudo ip netns exec tunnel6 sudo -u YOUR_USERNAME firefox
[/code]

You may run into problems as some browsers may corrupt their configuration when you run more than one instance of them. Your browser may not even notice that you started it several times as the communication between the instances may be blocked through the network namespaces. To prevent this issue, you should define another profile / data directory when you start your browser. If you are using chrome, you can do this the following way:

[code=bash]
sudo ip netns exec tunnel6 sudo -u YOUR_USERNAME google-chrome --user-data-dir=$HOME/.config/google-chrome-ipv6
[/code]

In some cases it might also be useful to a start a bash-session inside the new namespace in order to check if everything works correctly. This can be done by executing

[code=bash]
sudo ip netns exec tunnel6 bash
[/code]

[subsection]Using a SOCKS5 proxy[/subsection]

An other and easier way to handle the two distinct namespaces is to use a SOCKS5 proxy. By first creating the listening socket in the normal namespace and then switching to the IPv6 namespace it is possible to create another communication channel between them. By using this method all your programs can stay in your regular namespace, you simply have to enter the proxy details into each desired program which should use IPv6.

Using this method an outgoing packet takes this way when using a tunnel broker:

[code=text]program -> socks5 -> tun -> tun6v0 -> tun6v1 -> br0 -> eth0 -> router -> tunnel broker[/code]

The proxy will listen for connections in the regular namespace (IPv4) and translate them to IPv6 connections in the tunnel ns. This of course only works if the programs do not resolve the dns names on their own but send the names to the proxy.

When using this method for browsing the web: This is the default behaviour for Chrome, but not for Firefox. You can force Firefox to let the proxy resolve the names by opening [code]about:config[/code] and changing the value for [code]network.proxy.socks_remote_dns[/code] to true. You need to restart Firefox to make this option take effect. I would also recommend to choose a different skin / design for your IPv6 enabled browser, so that you can easily distinguish between both of them.

While all this is pretty easy to realize in theory, we didn't find any simple SOCKS5 proxy server supporting all these features - so we quickly wrote one ourself in Python.

[b][color=gray][UPDATE 09.07.2013][/color][/b] Added an HTTP proxy, as unfortunately not all programs support SOCKS5. [b][color=gray][/UPDATE][/color][/b]

[download]
[* url="/cms/downloads/ipv6/socks5-v1.0.0.tar.gz" md5="5e16ef5462c672f8ae37b75097011e00" sha256="ec1c65eef0b71813ec415f7a901a785b9c9eab2d5c8599125d725158bb115106" size="4,7 KB"]SOCKS5 proxy written for Python 2.6

[* url="/cms/downloads/ipv6/http-v1.0.2.tar.gz" md5="884c97b90515fa98770a5cd9953836ff" sha256="3073a97ec05d5bd59e2da836a72efcbc15ce7c0e08ce0a47e0b82436dd43d2b4" size="4,5 KB"]HTTP proxy written for Python 2.6
[/download]

The program doesn't support a lot of features, but should be exactly sufficient for our usecase!

[code=text]
usage: socks5.py [-h] [--fallback4] [--quiet] [--setns NAME] HOST PORT

Minimal IPv6 SOCKS5 proxy written in Python - v1.0.0

positional arguments:
  HOST          Interface to listen on (for example 'localhost')
  PORT          Port to listen on (for example 1080)

optional arguments:
  -h, --help    show this help message and exit
  --fallback4   Enabled IPv4 fallback
  --quiet       Disables logging to console
  --setns NAME  Switch network namespace after startup (requires root)

[ Written by FDS-Team - Web: http://fds-team.de/ - License: PUBLIC DOMAIN ]
[/code]

An usage example would be:

[code=bash]
sudo ./socks5.py --setns tunnel6 127.0.0.1 1080
[/code]

This first opens port 1080 in the current namespace and then switchs to the [code]tunnel6[/code]-namespace afterwards. This allows you to use the proxy in your current namespace while your tunnel device is in a different ns. The program also automatically drops the root-rights after everything is started to prevent any security holes. Please note that there is no additional authentication necessary to get access to the proxy - if it is reachable in the IPv4 network, you can use it, so you should only bind it to local addresses.

Of course you can also use this program for any other purpose, but you should keep in mind: The program was designed to only provide IPv6 support by default. If you want to fallback to IPv4 if there is no IPv6 address assigned to a dns name or an IPv4 address was sent to the proxy, please append [code]--fallback4[/code] to the command line. Otherwise the proxy will simply reject the connection request. Moreover this proxy always tries to prefer IPv6 over IPv4 whenever both variants are possible.

[b]Frequently asked questions:[/b]
[faq]
[*="The program terminates with \"OSERROR: 1\" inside switchNetNamespace"] This is caused by unsufficient privileges when starting the program without root-rights. Please just execute it as root and everything should work as it should. Don't worry, the program will drop the root-rights as soon as possible to prevent security holes.

[*="The program terminates with \"socket.error: [Errno 98] Address already in use\""] Either there is another program running on this port, or the operating system doesn't have completely cleaned up this port - in the second case just wait a few seconds, then it should work again.
[/faq]


[section]All in one (the easy way)[/section]

We have to admit, that all this configuration is not as easy as it sounds and it is especially annoying to enter all the commands over and over again after each reboot, so we decided to create simple to use scripts. They are configured for our enviroment and you may need to change some options (i.e. tunnel broker program) to fit your needs. The intention of these scripts is to provide an easy way to surf the IPv6 network with a separate browser window.

[b][color=gray][UPDATE 13.07.2013][/color][/b] Added alternative scripts working without bridge device but instead with iptables rules. [b][color=gray][/UPDATE][/color][/b]

[download]
[* url="/cms/downloads/ipv6/tunnel-v1.0.2.tar.gz" md5="1cd8881b5547cb6eb5568e92b2519cf2" sha256="ccc734fd5e25933fecc1c3605d73e0562fd46617a4289d66f20df0e607b10236" size="3,0 KB"]Tunnel scripts using bridge (requires [code]socks5.py[/code] and [code]http.py[/code])

[* url="/cms/downloads/ipv6/nat-tunnel-v1.0.0.tar.gz" md5="252374a9c5f04cced966560463722353" sha256="54bf10eab8f10a57d69af1b3d0c2994b1d860af768c59183dd3afc73a9c05e60" size="3,6 KB"]Alternative tunnel scripts using NAT (requires [code]socks5.py[/code] and [code]http.py[/code])

[/download]

The most interesting script might be [code]tunnel.sh[/code], which is reponsible for setting up all the needed interfaces, tunnels and the SOCKS proxy server. It is necessary to download the SOCKS and HTTP proxy script from above and put it in the same directory as the rest of the scripts, to get it working. You can use [code]sudo ./tunnel.sh start[/code] to bring everything up and [code]sudo ./tunnel.sh stop[/code] to disable the tunnel and shutdown all programs inside the namespace. You first need to change the options to your needs (this is the configuration for the bridge scripts, the other one has similar configuration options):

[code=bash]
# -- BEGIN CONFIGURATION

# Network name and namespace configuration
ETHNAME="eth0"		# Name of your ethernet device
VETHNAME="tun6v"	# Virtual ethernet device created by the script
BRNAME="br0"		# Name of the bridge created by the script
NETNSNAME="tunnel6"	# Network namespace containing our tunnel

# Change this to "/usr/sbin/aiccu start" if you are using aiccu
STARTTUNNEL="/usr/sbin/gogoc"

# Arguments for the socks5 proxy
SOCKSENABLE="yes"
SOCKSARGS=""
SOCKSPORT="1080"

# Arguments for the http proxy
HTTPENABLE="yes"
HTTPARGS=""
HTTPPORT="8080"

# -- END CONFIGURATION
[/code]

The most important options may be [code]ETHNAME[/code] which must be your normal ethernet interface and [code]STARTTUNNEL[/code] which defines your tunnel broker's client. Examples paths for Freenet6 (gogoc) and (aiccu) SixXS are included in the configuration above. You should also pay attention to [code]SOCKSARGS[/code] as the SOCKS proxy is configured to serve only IPv6 which may cause problems with your browser. Your browser may not be able to update itself and your adblocker may not work properly or slow down your connection, because it is unable to get the newest block lists. You can solve this by setting the option to [code]SOCKSARGS="--fallback4"[/code]. With this option set, your browser will take IPv6 if possible and fallback to IPv4 if necessary. Moreover you may have noticed that our script assumes the usage of a tunnel broker and dhcp client inside your network. If this is not the case for you, you manually have to change the code of the script.

The [code]tunnel.sh[/code]-script moreover provides a status command which can be called via [code]sudo ./tunnel.sh status[/code] and displays all important information about the status of your tunnel - this might be especially useful to find errors if something is not working correctly.

When the tunnel is up, you can either setup the applications of your choice to use the proxy server [code]127.0.0.1:1080[/code] or in case you also want to surf the web using the Google-Chrome browser, just call [code]./chrome.sh[/code] which already contains the commandline to start a second instance of chrome using the SOCKS5-proxy. As mentioned above it might be useful to use a different design theme to distingush both of them.

[section]Conclusion[/section]

The method of resource separation using namespaces allows to decide what each individual process may do and makes some configurations possible which would otherwise be impossible or much harder to achieve. The linux kernel does not just support network namespaces as used in this article, but also other kinds of namespaces which are just for different resources. Nevertheless, we should keep in mind that separating IPv6 can only be a temporary solution, IPv6 is the future and will become standard. We can only hope that software developers will make their software more secure so that we do not need this separation any more.
